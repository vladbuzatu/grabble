package com.example.vlad.locationapp;
import org.junit.Test;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class FirstPageActivityUnitTest {
    @Test
    public void emailValidator_CorrectEmailSimple_ReturnsTrue() {
        assertThat(FirstPageActivity.isValidEmailAddress("name@email.co.uk"), is(true));
        assertThat(FirstPageActivity.isValidEmailAddress(("buzatu.vlad@yahoo.com")), is(true));
        assertThat(FirstPageActivity.isValidEmailAddress("idgmail.co.uk.ro"), is(false));
        assertThat(FirstPageActivity.isValidEmailAddress("invalidaddress"), is(false));
    }

}
