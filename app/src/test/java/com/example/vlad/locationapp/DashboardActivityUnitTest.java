package com.example.vlad.locationapp;

/**
 * Created by vlad on 26/01/17.
 */

import junit.framework.Assert;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class DashboardActivityUnitTest {


    @Test
    public void check_computeScore(){
        assertTrue(DashboardActivity.computeScore("LOOKING") == 70);
        assertTrue(DashboardActivity.computeScore("GRABBLE") == 81);
        assertTrue(DashboardActivity.computeScore("MOIETER") == 35);
        assertTrue(DashboardActivity.computeScore("BEVERLY") == 78);
    }


}
