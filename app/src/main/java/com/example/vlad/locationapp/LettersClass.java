package com.example.vlad.locationapp;

import java.util.HashMap;

public class LettersClass {

    private static HashMap<Character, Integer> myLetters = new HashMap<Character, Integer>();
    private static HashMap<String, Boolean> collectedCoordinates = new HashMap<String, Boolean>();

    private static LettersClass lettersClass = new LettersClass();

    private LettersClass() {}

    public static LettersClass getInstance() {
        return lettersClass;
    }

    protected void setLetters (HashMap<Character, Integer> letters) {
        myLetters = letters;
    }

    protected void addLetter(Character letter) {

        if (myLetters.containsKey(letter))
            myLetters.put(letter, myLetters.get(letter) + 1);
        else
            myLetters.put(letter, 1);
    }

    protected void setCollectedCoordinates (HashMap<String, Boolean> coordinates) {
        collectedCoordinates = coordinates;
        System.out.println("Collected coordinates set..");
        System.out.println(collectedCoordinates.size());
    }

    protected void addCoordinates (String name) {
        collectedCoordinates.put(name, true);
    }

    protected void removeCoordinates(String name) {
        collectedCoordinates.remove(name);
    }

    protected static boolean checkCoordinates(String name) {
        if (collectedCoordinates.containsKey(name))
            return true;
        return false;
    }

    protected void removeLetter(Character letter) {
        if (myLetters.containsKey(letter)) {
            myLetters.put(letter, myLetters.get(letter) - 1);
            if (myLetters.get(letter) == 0)
                myLetters.remove(letter);
        }
    }

    protected static int getOccurences(Character letter) {
        if (myLetters.containsKey(letter))
            return myLetters.get(letter);
        return 0;
    }

    protected void resetLetters() {
        myLetters.clear();
    }

    protected void resetCoordinates() {
        collectedCoordinates.clear();
    }

    protected static HashMap<Character, Integer> getLetters() {
        return myLetters;
    }

    protected static HashMap<String, Boolean> getCollectedCoordinates() {
        return collectedCoordinates;
    }

}
