package com.example.vlad.locationapp;

import java.util.ArrayList;

public class WordsClass {

    private static ArrayList<String> words = new ArrayList<String>();

    private static WordsClass wordsClass = new WordsClass();

    private WordsClass() { }

    public static WordsClass getInstance() { return wordsClass; }

    protected static void addWord (String word) {
        words.add(word);
    }

    protected static void removeWord (String word) {
        int i;
        for (i = 0; i < words.size(); i++)
            if (words.get(i).equals(word))
                break;
        words.remove(i);
    }

}
