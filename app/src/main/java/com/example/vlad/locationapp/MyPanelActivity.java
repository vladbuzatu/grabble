package com.example.vlad.locationapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MyPanelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_panel);
        setUserDetails();

        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        int loyaltyPoints = myPrefs.getInt("loyaltyPoints", 0);

        TextView tv = (TextView) findViewById(R.id.textView10);
        tv.setText("Loyalty points gained so far: " + loyaltyPoints);

    }

    protected void exchange (View view) {
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        int loyaltyPoints = myPrefs.getInt("loyaltyPoints", 0);
        int value;

        EditText et = (EditText) findViewById(R.id.editText);
        try {
            value = Integer.parseInt(String.valueOf(et.getText()));
        }
        catch (NumberFormatException e1)
        {
            value = 0;
            e1.printStackTrace();
        }

        if (value <= loyaltyPoints) {
            Toast.makeText(getApplicationContext(), "Points exchanged!\n" +
                    value/2 + " game points earned.",
                    Toast.LENGTH_SHORT).show();
            int gameScore = myPrefs.getInt("gameScore", 0);
            gameScore += value/2;
            e.putInt("gameScore", gameScore);
            loyaltyPoints -= value;
            e.putInt("loyaltyPoints", loyaltyPoints);
            e.commit();
            TextView tv = (TextView) findViewById(R.id.textView10);
            tv.setText("Loyalty points gained so far: " + loyaltyPoints);
            et.setText("");
        }
        else
            Toast.makeText(getApplicationContext(), "Not enough loyalty points!",
                    Toast.LENGTH_SHORT).show();
    }

    public void setUserDetails(){

        System.out.println("Setting user details..");

        TextView userName = (TextView) findViewById(R.id.userName);
        TextView userEmail = (TextView) findViewById(R.id.userEmail);
        ProfilePictureView profilePictureView = (ProfilePictureView) findViewById(R.id.image);

        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String name =  myPrefs.getString("userName", "");
        String email = myPrefs.getString("userEmail", "");

        userName.setText(name.toString());
        userEmail.setText(email.toString());

        if (myPrefs.getString("accountType", "").equals("facebookAccount")) {
            System.out.println("Account type: " + myPrefs.getString("accountType", ""));
            System.out.println("Fb id: " + myPrefs.getString("userID", ""));
            String userID = myPrefs.getString("userID", "");
            profilePictureView.setProfileId(userID);
        }

    }

    protected void resetGame(View view) {
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = myPrefs.edit();

        editor.clear();
        editor.commit();

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myCollectedCoordinates.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            HashMap<String, Boolean> collectedCoordinates = new HashMap<>();
            oos.writeObject(collectedCoordinates);
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            HashMap<Character, Integer> myLetters = new HashMap<>();
            oos.writeObject(myLetters);
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            HashMap<String, Integer> myWords = new HashMap<>();
            oos.writeObject(myWords);
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myBadges.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            List<Integer> myBadges = new ArrayList<Integer>(Arrays.asList(1,1,1,1,1,1,1,1,1,1,1,1,1,1));
            oos.writeObject(myBadges);
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }

        LoginManager.getInstance().logOut();

        Intent intent = new Intent(this,FirstPageActivity.class);
        startActivity(intent);

    }
}
