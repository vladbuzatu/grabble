package com.example.vlad.locationapp;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static com.example.vlad.locationapp.R.drawable.checked;
import static com.example.vlad.locationapp.R.drawable.getreward;
import static com.example.vlad.locationapp.R.drawable.lock;

public class MyBadgesActivity extends AppCompatActivity {

    private List<Integer> goalNumbers = Arrays.asList(100, 500, 2500, 5000, 1, 10, 25, 100, 1000, 10000, 2500, 10000, 26, 50);
    private List<Integer> awardedPoints = Arrays.asList(10, 100, 250, 500, 10, 150, 300, 1000, 200, 1000, 100, 250, 100, 250);
    private List<String> goalMessages = Arrays.asList(
            "Walk 100 meters while exploring the map.",
            "Walk 500 meters while exploring the map.",
            "Walk 2.5 kilometers while exploring the map.",
            "Walk 5 kilometers while exploring the map.",
            "Create a word.",
            "Create 10 words",
            "Create 25 words.",
            "Create 100 words.",
            "Get 1k game points.",
            "Get 10k game points",
            "Get 2.5k loyalty points.",
            "Get 10k loyalty points.",
            "Have all letters (A-Z) collected.",
            "Collect 50 letters."
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_badges);

        updateStatus();
        final List<Integer> status = getStatus();

        LinearLayout ll = (LinearLayout) findViewById(R.id.linearLayout);

        Display display = getWindowManager().getDefaultDisplay();
        double width = display.getWidth();

        RelativeLayout.LayoutParams rightCVParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rightCVParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        RelativeLayout.LayoutParams leftCVParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        leftCVParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        RelativeLayout.LayoutParams rightImageSize = new RelativeLayout.LayoutParams((int) width*43/100, (int) width*43/100);

        RelativeLayout.LayoutParams leftImageSize = new RelativeLayout.LayoutParams((int) width*43/100, (int) width*43/100);

        RelativeLayout.LayoutParams lockLayout = new RelativeLayout.LayoutParams(200, 200);
        lockLayout.addRule(RelativeLayout.CENTER_IN_PARENT);

        RelativeLayout.LayoutParams rewardLayout = new RelativeLayout.LayoutParams(400, 400);
        rewardLayout.addRule(RelativeLayout.CENTER_IN_PARENT);

        for (int i = 0; i < 14; i+=2){
            final int j = i;
// RIGHT
            final RelativeLayout rlRight = new RelativeLayout(getApplicationContext());
            rlRight.setLayoutParams(rightCVParams);
            rlRight.setBackgroundColor(Color.WHITE);

            CardView cvRight = new CardView((getApplicationContext()));
            cvRight.setLayoutParams(rightCVParams);
            cvRight.setUseCompatPadding(true);

            if (status.get(i+1) == 1 || status.get(i+1) == 2) {
                Drawable drawableRight = getResources().getDrawable(getResources()
                        .getIdentifier("lach" + String.valueOf(i + 2), "drawable", getPackageName()));

                if (status.get(i+1) == 1) {
                    final ImageView lockImageRight = new ImageView(getApplicationContext());
                    lockImageRight.setImageResource(lock);
                    lockImageRight.setLayoutParams(lockLayout);

                    ImageView rightImage = new ImageView(getApplicationContext());
                    rightImage.setLayoutParams(rightImageSize);
                    rightImage.setImageDrawable(drawableRight);
                    rightImage.setPadding(50, 50, 50, 50);
                    rightImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getApplicationContext(), "To get this badge you have to: " + goalMessages.get(j+1),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                    rlRight.addView(rightImage);
                    rlRight.addView(lockImageRight);

                }
                else
                if (status.get(i+1) == 2){
                    final ImageView rewardImageRight = new ImageView(getApplicationContext());
                    rewardImageRight.setImageResource(getreward);
                    rewardImageRight.setLayoutParams(rewardLayout);

                    final ImageView rightImage = new ImageView(getApplicationContext());
                    rightImage.setLayoutParams(rightImageSize);
                    rightImage.setImageDrawable(drawableRight);
                    rightImage.setPadding(50, 50, 50, 50);
                    rightImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (status.get(j+1) == 2) {
                                status.set(j + 1, 3);
                                rewardImageRight.setVisibility(View.GONE);
                                Drawable drawableRight = getResources().getDrawable(getResources()
                                        .getIdentifier("ach" + String.valueOf(j + 2), "drawable", getPackageName()));
                                rightImage.setImageDrawable(drawableRight);
                                SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
                                addGameScore(awardedPoints.get(j + 1));
                                Toast.makeText(getApplicationContext(), awardedPoints.get(j + 1) + " game points awarded!",
                                        Toast.LENGTH_SHORT).show();
                                saveStatus(status);
                            }


                        }
                    });
                    rlRight.addView(rightImage);
                    rlRight.addView(rewardImageRight);
                }


            }
            else if (status.get(i+1) == 3){
                Drawable drawableRight = getResources().getDrawable(getResources()
                        .getIdentifier("ach" + String.valueOf(i + 2), "drawable", getPackageName()));
                final ImageView rightImage = new ImageView(getApplicationContext());
                rightImage.setLayoutParams(rightImageSize);
                rightImage.setImageDrawable(drawableRight);
                rightImage.setPadding(50, 50, 50, 50);
                rightImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), goalMessages.get(j+1),
                                Toast.LENGTH_LONG).show();
                    }
                });
                rlRight.addView(rightImage);
            }
            cvRight.addView(rlRight);



// LEFT
            CardView cvLeft = new CardView(getApplicationContext());
            cvLeft.setLayoutParams(leftCVParams);
            cvLeft.setUseCompatPadding(true);

            RelativeLayout rlLeft = new RelativeLayout(getApplicationContext());
            rlLeft.setLayoutParams(leftCVParams);
            rlLeft.setBackgroundColor(Color.WHITE);

            if (status.get(i) == 1 || status.get(i) == 2) {

                Drawable drawableLeft = getResources().getDrawable(getResources()
                        .getIdentifier("lach" + String.valueOf(i + 1), "drawable", getPackageName()));

                if (status.get(i) == 1) {

                    final ImageView lockImageLeft = new ImageView(getApplicationContext());
                    lockImageLeft.setImageResource(lock);
                    lockImageLeft.setLayoutParams(lockLayout);

                    ImageView leftImage = new ImageView(getApplicationContext());
                    leftImage.setLayoutParams(leftImageSize);
                    leftImage.setImageDrawable(drawableLeft);
                    leftImage.setPadding(50, 50, 50, 50);
                    leftImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getApplicationContext(), "To get this badge you have to: " + goalMessages.get(j),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                    rlLeft.addView(leftImage);
                    rlLeft.addView(lockImageLeft);
                } else if (status.get(i) == 2) {

                    final ImageView rewardImageLeft = new ImageView(getApplicationContext());
                    rewardImageLeft.setImageResource(getreward);
                    rewardImageLeft.setLayoutParams(rewardLayout);

                    final ImageView leftImage = new ImageView(getApplicationContext());
                    leftImage.setLayoutParams(leftImageSize);
                    leftImage.setImageDrawable(drawableLeft);
                    leftImage.setPadding(50, 50, 50, 50);
                    leftImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (status.get(j) == 2) {
                                status.set(j, 3);
                                rewardImageLeft.setVisibility(View.GONE);
                                Drawable drawableRight = getResources().getDrawable(getResources()
                                        .getIdentifier("ach" + String.valueOf(j + 1), "drawable", getPackageName()));
                                leftImage.setImageDrawable(drawableRight);
                                addGameScore(awardedPoints.get(j));
                                Toast.makeText(getApplicationContext(), awardedPoints.get(j) + " game points awarded!",
                                        Toast.LENGTH_SHORT).show();
                                saveStatus(status);
                            }
                        }
                    });

                    rlLeft.addView(leftImage);
                    rlLeft.addView(rewardImageLeft);
                }
            }
            else if (status.get(i) == 3){

                Drawable drawableLeft = getResources().getDrawable(getResources()
                        .getIdentifier("ach" + String.valueOf(i + 1), "drawable", getPackageName()));
                ImageView leftImage = new ImageView(getApplicationContext());
                leftImage.setLayoutParams(leftImageSize);
                leftImage.setImageDrawable(drawableLeft);
                leftImage.setPadding(50, 50, 50, 50);
                leftImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), goalMessages.get(j),
                                Toast.LENGTH_LONG).show();
                    }
                });

                rlLeft.addView(leftImage);
            }

            cvLeft.addView(rlLeft);

// FINAL
            RelativeLayout rl = new RelativeLayout(getApplicationContext());
            rl.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));

            rl.addView(cvRight);
            rl.addView(cvLeft);


            ll.addView(rl);

        }

    }

    private void addGameScore(int score) {
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = myPrefs.edit();
        int gameScore = myPrefs.getInt("gameScore", 0);
        gameScore += score;
        editor.putInt("gameScore", gameScore);
        editor.commit();
    }

    private void updateStatus(){

        List<Integer> myValues = new ArrayList<Integer>();
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String distanceWalked = myPrefs.getString("distanceWalked","");
        Integer distance;
        try{
            Double distanceDouble = Double.parseDouble(distanceWalked);
            distance = distanceDouble.intValue();

        } catch (NumberFormatException e1){
            distance = 0;
        }
        for (int i = 0; i < 4; i++)
            myValues.add(distance);

        System.out.println("DISTANCE: " + distance);

        int wordsCount;
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            wordsCount = ((HashMap) objectInputStream.readObject()).size();
        } catch (Exception e) {
            wordsCount = 0;
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
                HashMap<String, Integer> words = new HashMap<>();
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(words);
                oos.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }

        for (int i = 0; i < 4; i++)
            myValues.add(wordsCount);

        int gameScore = myPrefs.getInt("gameScore", 0);
        for (int i = 0; i < 2; i++)
            myValues.add(gameScore);
        int loyaltyPoints = myPrefs.getInt("loyaltyPoints", 0);
        System.out.println("Loyalty points: " + loyaltyPoints);
        for (int i = 0; i < 2; i++)
            myValues.add(loyaltyPoints);

        int lettersCount = 0;
        int differentLetters = 0;
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            HashMap<String, Integer> myLetters = (HashMap) objectInputStream.readObject();
            differentLetters = myLetters.size();
            for (int value : myLetters.values())
                lettersCount += value;
        } catch (Exception e) {
            differentLetters = 0;
            lettersCount = 0;
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
                HashMap<String, Integer> words = new HashMap<>();
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(words);
                oos.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }

        myValues.add(differentLetters);
        myValues.add(lettersCount);

        List<Integer> status = getStatus();

        for (int i = 0; i < status.size(); i++)
            if (status.get(i) == 1)
                if (myValues.get(i) >= goalNumbers.get(i)){
                    status.set(i, 2);
                }

        saveStatus(status);
    }

    private List<Integer> getStatus() {
        List<Integer> status;
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myBadges.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            status = (ArrayList) objectInputStream.readObject();
        } catch (Exception e) {
            status = new ArrayList<Integer>(Arrays.asList(1,1,1,1,1,1,1,1,1,1,1,1,1,1));
            saveStatus(status);
            e.printStackTrace();
        }
        return status;
    }

    private void saveStatus(List<Integer> status){
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myBadges.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(status);
            oos.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
