package com.example.vlad.locationapp;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class TabbedActivity extends AppCompatActivity {

    private static List<String> textViews = Arrays.asList(
      "Welcome to Grabble!\n" +
              "     The aim of the game is to make seven-letter words out of the letters which have been collected. Each letter has a point value associated with it and a score is assigned to a word by summing the scores of the letters in the word.\n" +
              "     Letters can be collected by visiting their location. There is a different set of letters for each day  of  the  week. Letters  can  only  be  collected  once  each  day.",
            "     Letters can also be purchased from the Letters Store, using the loyalty points gained while playing. Each time you walk while exploring the application's map, you earn loyalty points.\n" +
                    "     The words you have created are shown in My Words section. You can destroy an already formed word (by paying loyalty points) and reuse its letters to create higher score words.",
            "     In My Badges section you get tasks which bring you game points once completed. Also, there is a statistics section which highlights your verall game performance.\n" +
                    "\n" +
                    "Enjoy playing!"
    );


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabbed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);


            DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;

            RelativeLayout.LayoutParams rightImageSize = new RelativeLayout.LayoutParams((int) width*47/100, (int) width);
            rightImageSize.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            rightImageSize.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            RelativeLayout.LayoutParams leftImageSize = new RelativeLayout.LayoutParams((int) width*47/100, (int) width);
            leftImageSize.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            leftImageSize.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);


            Drawable drawableRight = getResources().getDrawable(getResources()
                    .getIdentifier("tutorial" + (2*getArguments().getInt(ARG_SECTION_NUMBER) ), "drawable", "com.example.vlad.locationapp"));

            Drawable drawableLeft = getResources().getDrawable(getResources()
                    .getIdentifier("tutorial" + (2*getArguments().getInt(ARG_SECTION_NUMBER) -1), "drawable", "com.example.vlad.locationapp"));

            ImageView imVLeft = (ImageView) rootView.findViewById(R.id.imageView);
            imVLeft.setLayoutParams(leftImageSize);
            imVLeft.setImageDrawable(drawableLeft);
            imVLeft.setPadding(0,0,0,100);

            ImageView imVRight = (ImageView) rootView.findViewById(R.id.imageView2);
            imVRight.setLayoutParams(rightImageSize);
            imVRight.setImageDrawable(drawableRight);
            imVRight.setPadding(0,0,0,100);

            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(textViews.get(getArguments().getInt(ARG_SECTION_NUMBER) - 1));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
