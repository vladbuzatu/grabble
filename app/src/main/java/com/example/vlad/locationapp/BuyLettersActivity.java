package com.example.vlad.locationapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;

public class BuyLettersActivity extends AppCompatActivity {

    private static List<Integer> letterScore = Arrays.asList(3, 20, 13, 10, 1, 15, 18, 9, 5, 25, 22, 11, 14, 6, 4, 19, 24, 8, 7, 2, 12, 17, 23, 16, 26);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_letters);
        setLetters();

        EditText letterQuantity = (EditText) findViewById(R.id.letterQuantity);
        letterQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText quantity = (EditText) findViewById(R.id.letterQuantity);
                Button buyLetter = (Button) findViewById(R.id.buyButton);
                if (!quantity.getText().toString().matches(""))
                    buyLetter.setVisibility(View.VISIBLE);
                else
                    buyLetter.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setLetters() {
        LinearLayout linearLayoutHorizontalAM = (LinearLayout) findViewById(R.id.amLettersList);
        linearLayoutHorizontalAM.removeAllViews();
        LinearLayout linearLayoutHorizontalNZ = (LinearLayout) findViewById(R.id.nzLettersList);
        linearLayoutHorizontalNZ.removeAllViews();

        for (int i = 0; i < 26; i++) {
            final String str;
            str = Character.toString((char)(i + 65));
            final String colorString = "#16a085";
            final TextDrawable drawable = TextDrawable.builder()
                    .buildRound(str, Color.parseColor(colorString));

            final TextView letterSelected = (TextView) findViewById(R.id.letterSelected);
            final ImageView imV = new ImageView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(270, 270);
            imV.setLayoutParams(layoutParams);
            imV.setImageDrawable(drawable);
            imV.setPadding(10, 10, 10, 10);
            imV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardView cv = (CardView) findViewById(R.id.basketCardView);
                    cv.setVisibility(View.VISIBLE);
                    ImageView imageView = (ImageView) findViewById(R.id.selectedLetter);
                    TextDrawable drawableCopy = TextDrawable.builder()
                            .buildRound(str, Color.parseColor(colorString));
                    imageView.setImageDrawable(drawableCopy);
                    letterSelected.setText(str);
                }
            });
            if (i < 13)
                linearLayoutHorizontalAM.addView(imV);
            else
                linearLayoutHorizontalNZ.addView(imV);
        }
    }

    public void buyLetter(View view) {
        EditText quantityString = (EditText) findViewById(R.id.letterQuantity);
        TextView letterSelected = (TextView) findViewById(R.id.letterSelected);

        String letter = letterSelected.getText().toString();

        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        int loyaltyPoints = myPrefs.getInt("loyaltyPoints", 0);

        try{
            int quantity = Integer.parseInt(quantityString.getText().toString());
            System.out.println("Index: " +  ((int) letter.toUpperCase().charAt(0) - 65));
            int price = letterScore.get((int) letter.toUpperCase().charAt(0) - 65) * quantity / 2;
            if (price <= loyaltyPoints){
                loyaltyPoints -= price;
                e.putInt("loyaltyPoints", loyaltyPoints);
                e.commit();
                addLetter(letter, quantity);
                quantityString.setText("");
                Toast.makeText(getApplicationContext(), "Letter added!\n" + loyaltyPoints + " Loyalty points left.",
                        Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Not enough loyalty points!",
                        Toast.LENGTH_LONG).show();
                quantityString.setText("");
            }

        } catch (NumberFormatException e1){
            e1.printStackTrace();
        }

    }

    public void addLetter(String letter, int quantity){
        LettersClass lettersClass = LettersClass.getInstance();
        System.out.println("Letters:" + lettersClass.getLetters().containsKey(letter.charAt(0)));
        System.out.println("Letter: " + letter);

        for (int i = 0; i < quantity; i++)
            lettersClass.addLetter(letter.toUpperCase().charAt(0));

        System.out.println("Letters:" + lettersClass.getLetters().size());

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(lettersClass.getLetters());
            oos.close();
            System.out.println("Letters saved!");
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }


}
