package com.example.vlad.locationapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Arrays;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

public class FirstPageActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_first_page);
        loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("success");

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                try {
                                    String email = object.getString("email");
                                    String name = object.getString("name");
                                    String id = object.getString("id");

                                    saveDetails(name, email, "facebookAccount");

                                    SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
                                    SharedPreferences.Editor e = myPrefs.edit();
                                    e.putString("userID", id);
                                    e.commit();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }

            @Override
            public void onCancel() {

                System.out.println("canceled");
            }

            @Override
            public void onError(FacebookException e) {
                System.out.println("error");

            }
        });
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String login = myPrefs.getString("accountType","");
        if (AccessToken.getCurrentAccessToken() != null || !login.equals("")) {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        }
    }



    public void launchTutorial(View view) {
        Intent intent = new Intent(this, TabbedActivity.class);
        startActivity(intent);
    }

    public void createLocalAccount(View view) {

            Button tutButton = (Button) findViewById(R.id.tutorial_button);
            Button fbButton = (Button) findViewById(R.id.login_button);
            Button localAccountButton = (Button) findViewById(R.id.local_account_button);
            Button submitButton = (Button) findViewById(R.id.submit_button_first_page);
            Button cancelButton = (Button) findViewById(R.id.cancel_button_first_page);
            EditText nameInput = (EditText) findViewById(R.id.nameTextInput);
            EditText emailInput = (EditText) findViewById(R.id.emailTextInput);

            tutButton.setVisibility(View.INVISIBLE);
            fbButton.setVisibility(View.INVISIBLE);
            localAccountButton.setVisibility(View.INVISIBLE);
            submitButton.setVisibility(View.VISIBLE);
            cancelButton.setVisibility(View.VISIBLE);
            nameInput.setVisibility(View.VISIBLE);
            emailInput.setVisibility(View.VISIBLE);

    }

    public void saveDetails (String name, String email, String type) {
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        e.putString("userName", name);
        e.putString("userEmail", email);
        e.putString("accountType", type);
        e.commit();
    }

    public void goToDashboard(View view) {

        EditText nameInput = (EditText) findViewById(R.id.nameTextInput);
        EditText emailInput = (EditText) findViewById(R.id.emailTextInput);

        String name = nameInput.getText().toString();
        String email = emailInput.getText().toString();

        if (isValidEmailAddress(email) && name.length() > 0) {
            saveDetails(name, email, "localAccount");

            System.out.println("Redirecting to dashboard...");
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        }
        else if (name.length() == 0)
        {
            new AlertDialog.Builder(this)
                    .setTitle("Invalid name!")
                    .setMessage("Please enter your name.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(R.drawable.logo)
                    .show();
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle("Invalid email!")
                    .setMessage("Please enter a valid email address.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(R.drawable.logo)
                    .show();
        }

    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public void resetFirstPage(View view) {

        Button tutButton = (Button) findViewById(R.id.tutorial_button);
        Button fbButton = (Button) findViewById(R.id.login_button);
        Button localAccountButton = (Button) findViewById(R.id.local_account_button);
        Button submitButton = (Button) findViewById(R.id.submit_button_first_page);
        Button cancelButton = (Button) findViewById(R.id.cancel_button_first_page);
        EditText nameInput = (EditText) findViewById(R.id.nameTextInput);
        EditText emailInput = (EditText) findViewById(R.id.emailTextInput);

        tutButton.setVisibility(View.VISIBLE);
        fbButton.setVisibility(View.VISIBLE);
        localAccountButton.setVisibility(View.VISIBLE);
        submitButton.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);
        nameInput.setVisibility(View.INVISIBLE);
        emailInput.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
