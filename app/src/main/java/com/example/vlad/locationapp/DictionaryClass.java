package com.example.vlad.locationapp;

import java.util.HashMap;

public class DictionaryClass {

    private static boolean loaded;
    private static HashMap<String,Boolean> dictionaryWords = new HashMap<String,Boolean>();

    private static DictionaryClass dictionaryClass = new DictionaryClass();

    private DictionaryClass() { }

    public static DictionaryClass getInstance() {
        return dictionaryClass;
    }

    protected static void setLoaded() {
        loaded = true;
    }

    protected static boolean isLoaded() {
        return loaded;
    }

    protected static int getLength() {
        return dictionaryWords.size();
    }

    protected static void addWord(String word) {
        dictionaryWords.put(word, true);
    }

    protected static boolean checkWord(String word) {
        if (dictionaryWords.containsKey(word))
            return true;
        return false;
    }
}
