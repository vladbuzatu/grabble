package com.example.vlad.locationapp;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.example.vlad.locationapp.StackOverflowXmlParser;
import com.google.android.gms.vision.face.Landmark;

import org.xmlpull.v1.XmlPullParserException;

public class MapsActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static final String TAG = MapsActivity.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String DEBUG_TAG = "HttpExample";

    private double lastLng;
    private double lastLat;

    List<StackOverflowXmlParser.Entry> entryList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

        LettersClass lettersClass = LettersClass.getInstance();
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myCollectedCoordinates.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            lettersClass.setCollectedCoordinates((HashMap) objectInputStream.readObject());
        } catch (Exception e) {
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myCollectedCoordinates.txt");
                HashMap<String, Boolean> collectedCoordinates = new HashMap<String, Boolean>();
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(collectedCoordinates);
                oos.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        mGoogleApiClient.connect();
        System.out.println("Maps -> onCreate");

    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        Log.i(TAG, String.valueOf(mGoogleApiClient!=null));
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        System.out.println("Maps -> onConnected");
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askForPermissions();
        }
        else {

            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            if (location != null)
                handleNewLocation(location);

        }
    }

    private void askForPermissions(){
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                100);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == 100) {
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                System.out.println("Permissions granted!");
                try {
                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                    if (location != null)
                        handleNewLocation(location);
                }
                catch (SecurityException e){
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("Entered else condition..");
                new AlertDialog.Builder(this)
                        .setTitle("Application unable to run properly without location permissions granted.")
                        .setMessage("Allow location usage?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                               askForPermissions();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        }
    }


    private void getAndParseKML() {

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

        System.out.println("Current day: " + day);
        String stringUrl = "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/" + day.toLowerCase() + ".kml";

        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String currentDay =  myPrefs.getString("currentDay", "");

        System.out.println("Stored day:" + currentDay);

        if (!currentDay.equals(day.toLowerCase())){
            SharedPreferences.Editor e = myPrefs.edit();
            e.putString("currentDay", day.toLowerCase());
            e.commit();

            HashMap<String, Boolean> collectedCoordinates = new HashMap<>();
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myCollectedCoordinates.txt");
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(collectedCoordinates);
                oos.close();
            }
            catch(Exception e1) {
                e1.printStackTrace();
            }
        }

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadWebpageTask().execute(stringUrl);
        } else {
            System.out.println("No network connection available.");
        }

    }

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            System.out.println("Maps -> onPostExecute");

            LettersClass lettersClass = LettersClass.getInstance();
            for (int i = 0; i < entryList.size(); i++) {
                double currentLatitude = Double.parseDouble(entryList.get(i).getLatitude());
                double currentLongitude = Double.parseDouble(entryList.get(i).getLongitude());
                LatLng latLng = new LatLng(currentLatitude, currentLongitude);

                TextDrawable drawable = TextDrawable.builder()
                        .buildRound(entryList.get(i).getLetter(), Color.GREEN);

                if (!lettersClass.checkCoordinates(entryList.get(i).getName()))
                    mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title(entryList.get(i).getLetter()));
                else
                    mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title(entryList.get(i).getLetter())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));


            }
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(DEBUG_TAG, "The response is: " + response);
            is = conn.getInputStream();

            StackOverflowXmlParser stackOverflowXmlParser = new StackOverflowXmlParser();
            entryList = stackOverflowXmlParser.parse(is);


            return "Done";

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (XmlPullParserException e) {
            return "Xml Parser Exception";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    private void handleNewLocation(Location location) {

        addWalkedDistance(location);

        Log.d(TAG, location.toString());
        System.out.println("Handling new location...");

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        lastLat = currentLatitude;
        lastLng = currentLongitude;
        CameraPosition position = mMap.getCameraPosition();

        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(20);
        builder.target(latLng);

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askForPermissions();
        }
        else
            mMap.setMyLocationEnabled(true);

    }

    private void addWalkedDistance(Location location) {

        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        String lastLongitude =  myPrefs.getString("lastLongitude", "");
        String lastLatitude = myPrefs.getString("lastLatitude", "");
        int loyaltyPoints = myPrefs.getInt("loyaltyPoints", 0);

        if (lastLongitude != "" && lastLatitude != "") {

            String distanceWalked = myPrefs.getString("distanceWalked" , "");
            try{
                Double distance = Double.parseDouble(distanceWalked);
                distance +=  haversine(location.getLatitude(), location.getLongitude(), Double.parseDouble(lastLatitude), Double.parseDouble(lastLongitude));
                loyaltyPoints += 2 * haversine(location.getLatitude(), location.getLongitude(), Double.parseDouble(lastLatitude), Double.parseDouble(lastLongitude));
                e.putInt("loyaltyPoints", loyaltyPoints);
                e.putString("distanceWalked", distance.toString());
                e.commit();
                System.out.println("Distance walked:" + distance);

            } catch (NumberFormatException e1){
                e.putString("distanceWalked", "0");
                e.commit();
            }
        }

        e.putString("lastLatitude", String.valueOf(location.getLatitude()));
        e.putString("lastLongitude", String.valueOf(location.getLongitude()));
        e.commit();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    private void setUpMapIfNeeded() {
        System.out.println("Setting up map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        getAndParseKML();
        mMap = map;
        mGoogleApiClient.connect();
        map.getUiSettings().setZoomControlsEnabled(true);
    }

    public static double haversine(
            double lat1, double lng1, double lat2, double lng2) {
        int r = 6371; // average radius of the earth in km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = r * c;
        return d * 1000;
    }

    public void grabLetters(View view) {

        LettersClass lettersClass = LettersClass.getInstance();

        Log.i("Latitude:", Double.toString(lastLat));
        Log.i("Longitude:", Double.toString(lastLng));

        boolean collectedLetters = false;

        for (int i = 0; i < entryList.size(); i++) {
            double currentLatitude = Double.parseDouble(entryList.get(i).getLatitude());
            double currentLongitude = Double.parseDouble(entryList.get(i).getLongitude());
            LatLng latLng = new LatLng(currentLatitude, currentLongitude);
            double distance = haversine((float) currentLatitude, (float) currentLongitude, (float) lastLat, (float) lastLng);

            if (distance <= 20 && !lettersClass.checkCoordinates(entryList.get(i).getName())) {
                collectedLetters = true;
                lettersClass.addLetter(entryList.get(i).getLetter().charAt(0));
                lettersClass.addCoordinates(entryList.get(i).getName());
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(currentLatitude, currentLongitude))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                );
                Log.i("Letter added:", entryList.get(i).getLetter());
            }

        }

        if (!collectedLetters)
            Toast.makeText(getApplicationContext(), "No uncollected letters nearby!",
                    Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed(){
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();
        e.putString("lastLatitude", "");
        e.putString("lastLongitude", "");
        e.commit();

        LettersClass lettersClass = LettersClass.getInstance();
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(lettersClass.getLetters());
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myCollectedCoordinates.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(lettersClass.getCollectedCoordinates());
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }
        System.out.println("Going back..");
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }
}
