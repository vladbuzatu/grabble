package com.example.vlad.locationapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.facebook.login.widget.ProfilePictureView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.example.vlad.locationapp.R.color.Seagreen;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private static List<Integer> letterScore = Arrays.asList(3, 20, 13, 10, 1, 15, 18, 9, 5, 25, 22, 11, 14, 6, 4, 19, 24, 8, 7, 2, 12, 21, 17, 23, 16, 26);
    private boolean populatedAM = false;
    private boolean populatedNZ = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        LettersClass lettersClass = LettersClass.getInstance();

        setUserDetails();

        try
        {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            lettersClass.setLetters((HashMap)objectInputStream.readObject());
            System.out.println("Letters list set.");
        }
        catch(Exception e) {
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
                HashMap<Character, Integer> myLetters = new HashMap<Character, Integer>();
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(myLetters);
                oos.close();
                finish();
                startActivity(getIntent());
            }
            catch(Exception e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }

        setLettersList();
        loadDictionary();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    private HashMap<String, Integer> getWords(){
        HashMap<String, Integer> words = new HashMap<>();
        try
        {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            words = (HashMap)objectInputStream.readObject();
        }
        catch(Exception e) {
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(words);
                oos.close();
            }
            catch(Exception e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }
        return words;
    }

    public void setUserDetails(){

        System.out.println("Setting user details..");

        NavigationView navView= (NavigationView) findViewById(R.id.nav_view);
        View header=navView.getHeaderView(0);

        TextView userName = (TextView) header.findViewById(R.id.userName);
        TextView userEmail = (TextView) header.findViewById(R.id.userEmail);
        ProfilePictureView profilePictureView = (ProfilePictureView) header.findViewById(R.id.image);

        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String name =  myPrefs.getString("userName", "");
        String email = myPrefs.getString("userEmail", "");

        userName.setText(name.toString());
        userEmail.setText(email.toString());

        if (myPrefs.getString("accountType", "").equals("facebookAccount")) {
            System.out.println("Account type: " + myPrefs.getString("accountType", ""));
            System.out.println("Fb id: " + myPrefs.getString("userID", ""));
            String userID = myPrefs.getString("userID", "");
            profilePictureView.setProfileId(userID);
        }

    }


    public void setLettersList() {

        LinearLayout linearLayoutHorizontal = (LinearLayout) findViewById(R.id.letters_horizontal_linear_layout);
        linearLayoutHorizontal.removeAllViews();
        LinearLayout linearLayoutHorizontal2 = (LinearLayout) findViewById(R.id.letters_horizontal_linear_layout2);
        linearLayoutHorizontal2.removeAllViews();
        LettersClass lettersClass = LettersClass.getInstance();
        HashMap<Character, Integer> myLetters = lettersClass.getLetters();

        for (Character letter = 'A'; letter <= 'Z'; letter++) {
            if (myLetters.containsKey(letter)) {
                final String str;
                str = Character.toString(letter);
                String colorString = "#16a085";
                TextDrawable drawable = TextDrawable.builder()
                        .buildRound(str, Color.parseColor(colorString));

                final ImageView imV = new ImageView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(270, 270);
                imV.setLayoutParams(layoutParams);
                imV.setImageDrawable(drawable);
                imV.setPadding(10, 10, 10, 10);
                imV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Log.i("Letter added:", str);
                        TextView tv = (TextView) findViewById(R.id.letters_inputted);
                        if (tv.getText().length() < 7) {
                            CharSequence lettersSequence = tv.getText();
                            lettersSequence = lettersSequence + str;
                            tv.setText(lettersSequence);
                            LettersClass lettersClass = LettersClass.getInstance();
                            lettersClass.removeLetter(str.charAt(0));
                            saveLetters();
                            if (lettersClass.getOccurences(str.charAt(0)) == 0)
                                imV.setVisibility(View.GONE);
                        }
                    }
                });
                if (letter < 'N') {
                    linearLayoutHorizontal.addView(imV);
                    populatedAM = true;
                }
                else {
                    linearLayoutHorizontal2.addView(imV);
                    populatedNZ = true;
                }
            }
        }

        if (!populatedAM) {
            TextView tv = new TextView(getApplicationContext());
            tv.setText("No letters between A and M.\n" +
                       "     Go grab some!");
            tv.setTextColor(getResources().getColor(Seagreen));
            tv.setTextSize(20);
            linearLayoutHorizontal.addView(tv);
        }

        if (!populatedNZ) {
            TextView tv = new TextView(getApplicationContext());
            tv.setTextColor(getResources().getColor(Seagreen));
            tv.setText("No letters between N and Z.\n" +
                    "     Go grab some!");
            tv.setTextSize(20);
            linearLayoutHorizontal2.addView(tv);
        }
    }

    private void saveLetters(){
        LettersClass lettersClass = LettersClass.getInstance();
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(lettersClass.getLetters());
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }
    }

    public void deleteLetter(View view){
        TextView tv = (TextView) findViewById(R.id.letters_inputted);
        CharSequence lettersSequence = tv.getText();
        if (lettersSequence.length() > 0) {
            tv.setText(lettersSequence.subSequence(0, lettersSequence.length() - 1));

            LettersClass lettersClass = LettersClass.getInstance();
            lettersClass.addLetter(lettersSequence.charAt(lettersSequence.length() - 1));

            setLettersList();
        }
    }

    public void goToMap(View view){
        putBackUnusedLetters();
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void goToBuyLetters(View view) {
        putBackUnusedLetters();
        Intent intent = new Intent(this, BuyLettersActivity.class);
        startActivity(intent);
    }

    public void checkWord(View view) {

        DictionaryClass dictionaryClass = DictionaryClass.getInstance();
        TextView tv = (TextView) findViewById(R.id.letters_inputted);
        String word = (String) tv.getText();
        Log.i("Checking word...", word);
        if (dictionaryClass.checkWord(word) && word.length() == 7){
            HashMap<String, Integer> words = getWords();
            if (!words.containsKey(word)) {
                Toast.makeText(getApplicationContext(), "Word created!\n" + computeScore(word) + " game points gained.",
                        Toast.LENGTH_LONG).show();
                words.put(word, computeScore(word));
                saveWords(words);
                tv.setText("");

                SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
                SharedPreferences.Editor e = myPrefs.edit();
                int gameScore = myPrefs.getInt("gameScore", 0);
                gameScore += computeScore(word);
                e.putInt("gameScore", gameScore);
                e.commit();
            }
            else
                Toast.makeText(getApplicationContext(), "The word already exists among created words.",
                        Toast.LENGTH_LONG).show();

        }
        else
            Toast.makeText(getApplicationContext(), "Invalid word!",
                    Toast.LENGTH_SHORT).show();

    }

    public static Integer computeScore (String word) {
        Integer score = 0;
        for (int i = 0; i < word.length(); i++) {
            System.out.println("Index: " + ((int) word.toUpperCase().charAt(i) - 65));
            score += letterScore.get((int) word.toUpperCase().charAt(i) - 65);
        }
        return score;
    }

    private void putBackUnusedLetters(){
        TextView tv = (TextView) findViewById(R.id.letters_inputted);
        String word = (String) tv.getText();
        tv.setText("");

        for (int i = 0; i < word.length(); i++) {
            LettersClass lettersClass = LettersClass.getInstance();
            lettersClass.addLetter(word.charAt(i));
            saveLetters();
            setLettersList();
        }
    }

    private void saveWords(HashMap<String, Integer> words) {
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(words);
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        putBackUnusedLetters();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        putBackUnusedLetters();
        // Handle navigation view item clicks here.
        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();

        int id = item.getItemId();

        if (id == R.id.nav_grab_letters) {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_buy_letters) {
            Intent intent = new Intent(this, BuyLettersActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_create_words) {

        } else if (id == R.id.nav_how_to_play) {
            Intent intent = new Intent(this, TabbedActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_statistics) {
            Intent intent = new Intent(this, StatisticsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_my_words) {
            Intent intent = new Intent(this, MyWordsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_statistics) {

        } else if (id == R.id.nav_my_badges){
            Intent intent = new Intent(this, MyBadgesActivity.class);
            startActivity(intent);
        } else if (id == R.id.myPanel) {
            Intent intent = new Intent(this, MyPanelActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadDictionary() {
        DictionaryClass dictionaryClass = DictionaryClass.getInstance();
        if (!dictionaryClass.isLoaded()){
            try {
                InputStream is = getAssets().open("grabble.txt");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                String text = new String(buffer);
                String word = "";
                for (int i = 0; i < text.length(); i++){
                    if (text.charAt(i) != '\n')
                        word += Character.toUpperCase(text.charAt(i));
                    else {
                        dictionaryClass.addWord(word);
                        word = "";
                    }

                }
                dictionaryClass.setLoaded();
            } catch (IOException e){
                throw new RuntimeException(e);
            }

        }
    }

}
