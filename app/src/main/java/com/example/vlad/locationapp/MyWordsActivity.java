package com.example.vlad.locationapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import static android.R.drawable.ic_delete;

public class MyWordsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_words);
        setWordsList();
    }

    private void setWordsList(){

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            final HashMap<String, Integer> words = (HashMap) objectInputStream.readObject();

            RelativeLayout.LayoutParams paramsCard = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            RelativeLayout.LayoutParams paramsWord = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            paramsWord.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            paramsWord.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            paramsWord.addRule(RelativeLayout.ALIGN_PARENT_START);

            RelativeLayout.LayoutParams paramsScore = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            RelativeLayout.LayoutParams paramsImage = new RelativeLayout.LayoutParams(110, 110);
            paramsImage.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            paramsImage.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            paramsImage.addRule(RelativeLayout.ALIGN_PARENT_END);

            LinearLayout ll = (LinearLayout) findViewById(R.id.myWordsScroll);

            if (words != null) {
                int i = 0;
                for (final String word : words.keySet()) {

                    final CardView cv = new CardView(this);
                    cv.setLayoutParams(paramsCard);
                    cv.setUseCompatPadding(true);

                    final RelativeLayout rv = new RelativeLayout(this);
                    rv.setLayoutParams(paramsCard);

                    final TextView wordField = new TextView(this);
                    wordField.setLayoutParams(paramsWord);
                    wordField.setTextSize(35);
                    wordField.setText(word);
                    wordField.setId(i);

                    final TextView score = new TextView(this);
                    score.setLayoutParams(paramsScore);
                    score.setTextSize(35);
                    score.setText(words.get(word).toString());
                    score.setPadding(580, 0, 0, 0);


                    final ImageView image = new ImageView(this);
                    image.setImageResource(ic_delete);
                    image.setLayoutParams(paramsImage);
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
                            SharedPreferences.Editor e = myPrefs.edit();
                            int loyaltyPoints = myPrefs.getInt("loyaltyPoints", 0);
                            int price = words.get(word) / 3;
                            if (price <= loyaltyPoints) {
                                int gameScore = myPrefs.getInt("gameScore", 0);
                                gameScore -= words.get(word);
                                e.putInt("gameScore", gameScore);
                                loyaltyPoints -= price;
                                e.putInt("loyaltyPoints", loyaltyPoints);
                                e.commit();
                                resetLetters(word);
                                cv.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Word destroyed!\nLetters added back to list.\n" + loyaltyPoints + " Loyalty points left.",
                                        Toast.LENGTH_LONG).show();
                                words.remove(wordField.getText());
                                saveWords(words);
                            }
                            else
                                Toast.makeText(getApplicationContext(), "Not enough loyalty points!",
                                        Toast.LENGTH_LONG).show();
                        }
                    });


                    rv.addView(wordField);
                    rv.addView(score);
                    rv.addView(image);

                    cv.addView(rv);

                    ll.addView(cv);
                }
            }

        } catch (Exception e) {
            try {
                File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
                HashMap<String, Integer> words = new HashMap<>();
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(words);
                oos.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }
    }

    public void resetLetters(String word) {
        LettersClass lettersClass = LettersClass.getInstance();
        System.out.println("Word: " + word);
        for (int i = 0; i < word.length(); i++) {
            System.out.println("Letter: " + word.charAt(i));
            lettersClass.addLetter(word.charAt(i));
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(lettersClass.getLetters());
            oos.close();
            System.out.println("Letters saved!");
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }
    }

    private void saveWords(HashMap<String, Integer> words) {
        try {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(words);
            oos.close();
        }
        catch(Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }


}
