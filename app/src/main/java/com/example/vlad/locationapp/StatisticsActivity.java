package com.example.vlad.locationapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.example.vlad.locationapp.R.drawable.facebook_share_button;

public class StatisticsActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private LoginManager manager;

    private List<String> stats = Arrays.asList(
            "Total Game Score",
            "Loyalty Points",
            "Overall Distance Walked",
            "Highest Word Score",
            "Total Number of Words",
            "Letters Collected");

    private List<Integer> numbers = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        setNumbers();

        LinearLayout ll = (LinearLayout) findViewById(R.id.statsList);
        RelativeLayout.LayoutParams cardViewLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        RelativeLayout.LayoutParams textViewLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        textViewLayout.leftMargin = 350;

        RelativeLayout.LayoutParams shareLayout = new RelativeLayout.LayoutParams(300,100);
        shareLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        shareLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        RelativeLayout.LayoutParams numberViewLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        numberViewLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        numberViewLayout.leftMargin = 350;

        for (int i = 0; i < 6; i++){

            RelativeLayout rl = new RelativeLayout(getApplicationContext());
            rl.setLayoutParams(cardViewLayout);
            rl.setBackgroundColor(Color.WHITE);

            CardView cv = new CardView(getApplicationContext());
            cv.setLayoutParams(cardViewLayout);
            cv.setUseCompatPadding(true);

            final ImageView imV = new ImageView(getApplicationContext());
            Drawable drawable = getResources().getDrawable(getResources()
                    .getIdentifier("stats" + String.valueOf(i+1), "drawable", getPackageName()));
            imV.setImageDrawable(drawable);
            imV.setLayoutParams(new RelativeLayout.LayoutParams(270,270));
            imV.setPadding(15,15,15,15);

            final TextView numberView = new TextView(getApplicationContext());
            numberView.setLayoutParams(numberViewLayout);
            numberView.setTextSize(25);
            numberView.setText(numbers.get(i).toString());
            numberView.setTextColor(Color.BLACK);

            final TextView tv = new TextView(getApplicationContext());
            tv.setLayoutParams(textViewLayout);
            tv.setTextColor(Color.BLACK);
            tv.setTextSize(17);
            tv.setText(stats.get(i));

            ImageView shareIcon = new ImageView(getApplicationContext());
            shareIcon.setImageResource(facebook_share_button);
            shareIcon.setLayoutParams(shareLayout);
            shareIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestPermission("Grabble: " + tv.getText() + ": " + numberView.getText(), ((BitmapDrawable)imV.getDrawable()).getBitmap());
                }
            });

            rl.addView(imV);
            rl.addView(shareIcon);
            rl.addView(tv);
            rl.addView(numberView);
            cv.addView(rl);
            ll.addView(cv);

        }

    }


    public void requestPermission(final String message, final Bitmap bitmapImage) {
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        List<String> permissionsNeeded = Arrays.asList("publish_actions");

        manager = LoginManager.getInstance();
        manager.logInWithPublishPermissions(this, permissionsNeeded);

        manager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                postOnFacebook(message, bitmapImage);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    public void postOnFacebook(String message, Bitmap bitmapImage){

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(bitmapImage)
                .setCaption(message)
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, null);

    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data)
    {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }

    private void setNumbers() {
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor e = myPrefs.edit();

        numbers.add(myPrefs.getInt("gameScore",0));
        numbers.add(myPrefs.getInt("loyaltyPoints", 0));

        System.out.println(myPrefs.getInt("loyaltyPoints", 0));

        String distanceWalked = myPrefs.getString("distanceWalked" , "");
        try{
            Double distance = Double.parseDouble(distanceWalked);
            numbers.add(distance.intValue());
        } catch (NumberFormatException e1){
            numbers.add(0);
            e.commit();
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myWords.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            HashMap<String, Integer> words = (HashMap) objectInputStream.readObject();
            numbers.add(Collections.max(words.values()));
            numbers.add(words.size());
        }
        catch (Exception e1) {
            numbers.add(0);
            numbers.add(0);
            e1.printStackTrace();
        }

        try {
            File file = new File(getApplicationContext().getFilesDir(), "myLetters.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            HashMap<String, Integer> letters = (HashMap) objectInputStream.readObject();
            int sum = 0;
            for (int value : letters.values())
                sum += value;
            numbers.add(sum);
        }
        catch (Exception e1) {
            numbers.add(0);
            e1.printStackTrace();
        }

        System.out.println(numbers.get(0));

    }

}
