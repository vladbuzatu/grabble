package com.example.vlad.locationapp;

/**
 * Created by vlad on 26/01/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

public class DictionaryTest {
    private DictionaryClass dictionaryClass = DictionaryClass.getInstance();


    @Before
    public void before() {
        Context context = InstrumentationRegistry.getTargetContext();

        try {
            InputStream is = context.getAssets().open("grabble.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer);
            String word = "";
            for (int i = 0; i < text.length(); i++){
                if (text.charAt(i) != '\n')
                    word += Character.toUpperCase(text.charAt(i));
                else {
                    dictionaryClass.addWord(word);
                    // Log.i("Word:", word);
                    word = "";
                }

            }
            dictionaryClass.setLoaded();
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    @Test
    public void check_word_occurence(){
        assertThat(dictionaryClass.checkWord("AARONIC"), is(true));
        assertThat(dictionaryClass.checkWord("GRABBLE"), is(true));
        assertThat(dictionaryClass.checkWord("INVALIDWord"), is(false));
    }

    @Test
    public void check_dictionary_size(){
        Assert.assertEquals(dictionaryClass.getLength() ,  23714);
    }

}
